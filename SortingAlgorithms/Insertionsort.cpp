#include "Headers/Insertionsort.h"

Insertionsort::Insertionsort() {}

vector<int> Insertionsort::insertionSortInt(vector<int> unsortedArray) {
    for (int index = 0; index < unsortedArray.size(); ++index) {
        int position = index;
        int temp_Value = unsortedArray[index];

        while (position > 0 && unsortedArray[position - 1] > temp_Value) {
            unsortedArray[position] = unsortedArray[position - 1];
            position--;
        }
        unsortedArray[position] = temp_Value;
    }
    return unsortedArray;
}

vector<char> Insertionsort::insertionSortChar(vector<char> unsortedArray) {
    for (int index = 0; index < unsortedArray.size(); ++index) {
        int position = index;
        char temp_Value = unsortedArray[index];

        while (position > 0 && unsortedArray[position - 1] > temp_Value) {
            unsortedArray[position] = unsortedArray[position - 1];
            position--;
        }
        unsortedArray[position] = temp_Value;
    }
    return unsortedArray;
}

vector<float> Insertionsort::insertionSortFloat(vector<float> unsortedArray) {
    for (int index = 0; index < unsortedArray.size(); ++index) {
        int position = index;
        float temp_Value = unsortedArray[index];

        while (position > 0 && unsortedArray[position - 1] > temp_Value) {
            unsortedArray[position] = unsortedArray[position - 1];
            position--;
        }
        unsortedArray[position] = temp_Value;
    }
    return unsortedArray;
}

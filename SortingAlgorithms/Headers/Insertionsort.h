#ifndef C___INSERTIONSORT_H
#define C___INSERTIONSORT_H

#include <vector>

using namespace std;

class Insertionsort{
public:
    Insertionsort();
    vector<int> insertionSortInt (vector<int> unsortedArray);
    vector<char> insertionSortChar (vector<char> unsortedArray);
    vector<float> insertionSortFloat (vector<float> unsortedArray);
};

#endif //C___INSERTIONSORT_H

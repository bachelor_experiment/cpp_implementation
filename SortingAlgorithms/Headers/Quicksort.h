#ifndef C___QUICKSORT_H
#define C___QUICKSORT_H

#endif //C___QUICKSORT_H

#include <vector>

using namespace std;

class Quicksort {
public:
    Quicksort();

    vector<int> startQuickSortInt(vector<int> unsortedArray);
    vector<float> startQuickSortFloat(vector<float> unsortedArray);
    vector<char> startQuickSortChar(vector<char> unsortedArray);

private:
    vector<int> intArray;
    vector<float> floatArray;
    vector<char> charArray;

    void quickSortInt(int leftPointer, int rightPointer);
    void quickSortFloat(int leftPointer, int rightPointer);
    void quickSortChar(int leftPointer, int rightPointer);

};
#ifndef C___SELECTIONSORT_H
#define C___SELECTIONSORT_H

#include <vector>

using namespace std;

class SelectionSort {
public:
    SelectionSort();
    vector<int> SelectionSortInt(vector<int> unsortedArray);
    vector<char> SelectionSortChar(vector<char> unsortedArray);
    vector<float> SelectionSortFloat(vector<float> unsortedArray);
};


#endif //C___SELECTIONSORT_H

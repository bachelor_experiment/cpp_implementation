//
// Created by casperbjork on 08/06/2020.
//

#ifndef C___MERGESORT_H
#define C___MERGESORT_H

#endif //C___MERGESORT_H

#include <vector>

using namespace std;

class Mergesort {
public:
    Mergesort();

    vector<int> mergeSortInt(vector<int> unsortedArray);
    vector<float> mergeSortFloat(vector<float> unsortedArray);
    vector<char> mergeSortChar(vector<char> unsortedArray);

private:
    vector<int> mergeVectorInt(vector<int> left_array, vector<int> right_array);
    vector<float> mergeVectorFloat(vector<float> left_array, vector<float> right_array);
    vector<char> mergeVectorChar(vector<char> left_array, vector<char> right_array);

};
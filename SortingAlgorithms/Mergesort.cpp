#include "Headers/Mergesort.h"
#include <cmath>

Mergesort::Mergesort() {

}

vector<int> Mergesort::mergeSortInt(vector<int> unsortedArray) {
    if (unsortedArray.size() < 2) {
        return unsortedArray;
    }

    int middleIndex = floor(unsortedArray.size()/2);
    vector<int> left_array (middleIndex, 0);
    for (int i = 0; i < middleIndex; ++i) {
        left_array[i] = unsortedArray[i];
    }

    vector<int> right_array (unsortedArray.size() - middleIndex, 0);
    int counter = 0;
    for (int j = middleIndex; j < unsortedArray.size(); ++j) {
        right_array[counter] = unsortedArray[j];
        counter++;
    }

    return mergeVectorInt(mergeSortInt(left_array), mergeSortInt(right_array));
}

vector<int> Mergesort::mergeVectorInt(vector<int> left_array, vector<int> right_array) {
    vector<int> resultArray (left_array.size() + right_array.size(), 0);
    int resultIndex = 0, leftIndex = 0, rightIndex = 0;

    while (leftIndex < left_array.size() && rightIndex < right_array.size()) {
        if(left_array[leftIndex] < right_array[rightIndex]) {
            resultArray[resultIndex] = left_array[leftIndex];
            leftIndex++;
        } else {
            resultArray[resultIndex] = right_array[rightIndex];
            rightIndex++;
        }
        resultIndex++;
    }

    //Adding the rest of elements that could have been left out.
    while(leftIndex < left_array.size()) {
        resultArray[resultIndex] = left_array[leftIndex];
        leftIndex++;
        resultIndex++;
    }

    while(rightIndex < right_array.size()) {
        resultArray[resultIndex] = right_array[rightIndex];
        rightIndex++;
        resultIndex++;
    }

    return resultArray;
}

vector<float> Mergesort::mergeSortFloat(vector<float> unsortedArray) {
    if (unsortedArray.size() < 2) {
        return unsortedArray;
    }

    int middleIndex = floor(unsortedArray.size()/2);
    vector<float> left_array (middleIndex, 0);
    for (int i = 0; i < middleIndex; ++i) {
        left_array[i] = unsortedArray[i];
    }

    vector<float> right_array (unsortedArray.size() - middleIndex, 0);
    int counter = 0;
    for (int j = middleIndex; j < unsortedArray.size(); ++j) {
        right_array[counter] = unsortedArray[j];
        counter++;
    }

    return mergeVectorFloat(mergeSortFloat(left_array), mergeSortFloat(right_array));
}

vector<float> Mergesort::mergeVectorFloat(vector<float> left_array, vector<float> right_array) {
    vector<float> resultArray (left_array.size() + right_array.size(), 0);
    int resultIndex = 0, leftIndex = 0, rightIndex = 0;

    while (leftIndex < left_array.size() && rightIndex < right_array.size()) {
        if(left_array[leftIndex] < right_array[rightIndex]) {
            resultArray[resultIndex] = left_array[leftIndex];
            leftIndex++;
        } else {
            resultArray[resultIndex] = right_array[rightIndex];
            rightIndex++;
        }
        resultIndex++;
    }

    //Adding the rest of elements that could have been left out.
    while(leftIndex < left_array.size()) {
        resultArray[resultIndex] = left_array[leftIndex];
        leftIndex++;
        resultIndex++;
    }

    while(rightIndex < right_array.size()) {
        resultArray[resultIndex] = right_array[rightIndex];
        rightIndex++;
        resultIndex++;
    }

    return resultArray;
}

vector<char> Mergesort::mergeSortChar(vector<char> unsortedArray) {
    if (unsortedArray.size() < 2) {
        return unsortedArray;
    }

    int middleIndex = floor(unsortedArray.size()/2);
    vector<char> left_array (middleIndex, 'a');
    for (int i = 0; i < middleIndex; ++i) {
        left_array[i] = unsortedArray[i];
    }

    vector<char> right_array (unsortedArray.size() - middleIndex, 'a');
    int counter = 0;
    for (int j = middleIndex; j < unsortedArray.size(); ++j) {
        right_array[counter] = unsortedArray[j];
        counter++;
    }

    return mergeVectorChar(mergeSortChar(left_array), mergeSortChar(right_array));
}

vector<char> Mergesort::mergeVectorChar(vector<char> left_array, vector<char> right_array) {
    vector<char> resultArray (left_array.size() + right_array.size(), 'a');
    int resultIndex = 0, leftIndex = 0, rightIndex = 0;

    while (leftIndex < left_array.size() && rightIndex < right_array.size()) {
        if(left_array[leftIndex] < right_array[rightIndex]) {
            resultArray[resultIndex] = left_array[leftIndex];
            leftIndex++;
        } else {
            resultArray[resultIndex] = right_array[rightIndex];
            rightIndex++;
        }
        resultIndex++;
    }

    //Adding the rest of elements that could have been left out.
    while(leftIndex < left_array.size()) {
        resultArray[resultIndex] = left_array[leftIndex];
        leftIndex++;
        resultIndex++;
    }

    while(rightIndex < right_array.size()) {
        resultArray[resultIndex] = right_array[rightIndex];
        rightIndex++;
        resultIndex++;
    }

    return resultArray;
}



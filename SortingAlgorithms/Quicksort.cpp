#include "Headers/Quicksort.h"
#include <cmath>

Quicksort::Quicksort() {

}

vector<int> Quicksort::startQuickSortInt(vector<int> unsortedArray) {
    this->intArray = unsortedArray;
    this->quickSortInt(0, this->intArray.size()-1);
    return this->intArray;
}

void Quicksort::quickSortInt(int leftPointer, int rightPointer) {
    int index = 0;
    int rightIndex = rightPointer;
    int leftIndex = leftPointer;

    if (this->intArray.size() > 1) {
        int pivot = this->intArray[floor((rightPointer + leftPointer)/2)];

        while (leftPointer <= rightPointer) {
            while (pivot > this->intArray[leftPointer]) {
                leftPointer++;
            }

            while (pivot < this->intArray[rightPointer]) {
                rightPointer--;
            }

            if (leftPointer <= rightPointer) {
                int temp = this->intArray[leftPointer];
                this->intArray[leftPointer] = this->intArray[rightPointer];
                this->intArray[rightPointer] = temp;
                leftPointer++;
                rightPointer--;
            }
        }
        index = leftPointer;

        if (leftIndex < index-1) {
            quickSortInt(leftIndex, index-1);
        }

        if (index < rightIndex) {
            quickSortInt(index, rightIndex);
        }
    }
}

vector<float> Quicksort::startQuickSortFloat(vector<float> unsortedArray) {
    this->floatArray = unsortedArray;
    this->quickSortFloat(0, this->floatArray.size()-1);
    return this->floatArray;
}

void Quicksort::quickSortFloat(int leftPointer, int rightPointer) {
    int index = 0;
    int rightIndex = rightPointer;
    int leftIndex = leftPointer;

    if (this->floatArray.size() > 1) {
        float pivot = this->floatArray[floor((rightPointer + leftPointer)/2)];

        while (leftPointer <= rightPointer) {
            while (pivot > this->floatArray[leftPointer]) {
                leftPointer++;
            }

            while (pivot < this->floatArray[rightPointer]) {
                rightPointer--;
            }

            if (leftPointer <= rightPointer) {
                float temp = this->floatArray[leftPointer];
                this->floatArray[leftPointer] = this->floatArray[rightPointer];
                this->floatArray[rightPointer] = temp;
                leftPointer++;
                rightPointer--;
            }
        }
        index = leftPointer;

        if (leftIndex < index-1) {
            quickSortFloat(leftIndex, index-1);
        }

        if (index < rightIndex) {
            quickSortFloat(index, rightIndex);
        }
    }
}

vector<char> Quicksort::startQuickSortChar(vector<char> unsortedArray) {
    this->charArray = unsortedArray;
    this->quickSortChar(0, this->charArray.size()-1);
    return this->charArray;
}

void Quicksort::quickSortChar(int leftPointer, int rightPointer) {
    int index = 0;
    int rightIndex = rightPointer;
    int leftIndex = leftPointer;

    if (this->charArray.size() > 1) {
        char pivot = this->charArray[floor((rightPointer + leftPointer)/2)];

        while (leftPointer <= rightPointer) {
            while (pivot > this->charArray[leftPointer]) {
                leftPointer++;
            }

            while (pivot < this->charArray[rightPointer]) {
                rightPointer--;
            }

            if (leftPointer <= rightPointer) {
                char temp = this->charArray[leftPointer];
                this->charArray[leftPointer] = this->charArray[rightPointer];
                this->charArray[rightPointer] = temp;
                leftPointer++;
                rightPointer--;
            }
        }
        index = leftPointer;

        if (leftIndex < index-1) {
            quickSortChar(leftIndex, index-1);
        }

        if (index < rightIndex) {
            quickSortChar(index, rightIndex);
        }
    }
}


#include "Headers/SelectionSort.h"

SelectionSort::SelectionSort() {

}

vector<int> SelectionSort::SelectionSortInt(vector<int> unsortedArray) {
    for (int i = 0; i < unsortedArray.size(); ++i) {
        int lowestValueIndex = i;
        for (int j = i + 1; j < unsortedArray.size(); ++j) {
            if (unsortedArray[lowestValueIndex] > unsortedArray[j]) {
                lowestValueIndex = j;
            }
        }

        if (lowestValueIndex != i) {
            int temp = unsortedArray[i];
            unsortedArray[i] = unsortedArray[lowestValueIndex];
            unsortedArray[lowestValueIndex] = temp;
        }
    }
    return unsortedArray;
}

vector<char> SelectionSort::SelectionSortChar(vector<char> unsortedArray) {
    for (int i = 0; i < unsortedArray.size(); ++i) {
        int lowestValueIndex = i;
        for (int j = i + 1; j < unsortedArray.size(); ++j) {
            if (unsortedArray[lowestValueIndex] > unsortedArray[j]) {
                lowestValueIndex = j;
            }
        }

        if (lowestValueIndex != i) {
            char temp = unsortedArray[i];
            unsortedArray[i] = unsortedArray[lowestValueIndex];
            unsortedArray[lowestValueIndex] = temp;
        }
    }
    return unsortedArray;
}

vector<float> SelectionSort::SelectionSortFloat(vector<float> unsortedArray) {
    for (int i = 0; i < unsortedArray.size(); ++i) {
        int lowestValueIndex = i;
        for (int j = i + 1; j < unsortedArray.size(); ++j) {
            if (unsortedArray[lowestValueIndex] > unsortedArray[j]) {
                lowestValueIndex = j;
            }
        }

        if (lowestValueIndex != i) {
            float temp = unsortedArray[i];
            unsortedArray[i] = unsortedArray[lowestValueIndex];
            unsortedArray[lowestValueIndex] = temp;
        }
    }
    return unsortedArray;
}


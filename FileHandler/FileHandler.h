#ifndef C___FILEHANDLER_H
#define C___FILEHANDLER_H
#include <string>
#include <vector>

using namespace std;

class FileHandler {
 public:
    FileHandler();
    FileHandler(string path);
    vector<int> ReadFileInt(string filename);
    vector<char> ReadFileChar(string filename);
    vector<float> ReadFileFloat(string filename);
 private:
    string PATH_TO_TESTDATA;
};



#endif //C___FILEHANDLER_H

#include "FileHandler.h"
#include <fstream>
#include <array>
#include <vector>

 FileHandler::FileHandler() {
    this->PATH_TO_TESTDATA = "";
}

FileHandler::FileHandler(string path) {
    this->PATH_TO_TESTDATA = path;
}

vector<int> FileHandler::ReadFileInt(string filename) {
    vector<int> unsortedArray;

    ifstream Datafile(PATH_TO_TESTDATA + filename);

    string inputRow = "";
    getline(Datafile, inputRow);
    while (getline(Datafile, inputRow)) {
        unsortedArray.push_back(stoi(inputRow));
    }

    Datafile.close();
    return unsortedArray;
}

vector<char> FileHandler::ReadFileChar(string filename) {
    vector<char> unsortedArray;

    ifstream Datafile(PATH_TO_TESTDATA + filename);

    string inputRow = "";
    getline(Datafile, inputRow);
    while (getline(Datafile, inputRow)) {
        unsortedArray.push_back(inputRow[0]);
    }

    Datafile.close();
    return unsortedArray;
}

vector<float> FileHandler::ReadFileFloat(string filename) {
    vector<float> unsortedArray;

    ifstream Datafile(PATH_TO_TESTDATA + filename);

    string inputRow = "";
    getline(Datafile, inputRow);
    while (getline(Datafile, inputRow)) {
        unsortedArray.push_back(stof(inputRow));
    }

    Datafile.close();
    return unsortedArray;
}

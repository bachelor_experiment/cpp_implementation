#include <iostream>
#include "FileHandler/FileHandler.h"
#include "SortingAlgorithms/Headers/SelectionSort.h"
#include "SortingAlgorithms/Headers/Insertionsort.h"
#include "SortingAlgorithms/Headers/Mergesort.h"
#include "SortingAlgorithms/Headers/Quicksort.h"
#include <chrono>

using namespace std;
using namespace chrono;

int main() {
    FileHandler fileHandler = FileHandler("/home/casperbjork/Development/Bachelor_Thesis/testData/");
    Quicksort quicksort = Quicksort();
    Mergesort mergesort = Mergesort();
    Insertionsort insertionsort = Insertionsort();
    SelectionSort selectionSort = SelectionSort();

    // -- CHANGE BELOW HERE --
    vector<int> unsortedArray = fileHandler.ReadFileInt("testInt_1.txt");
    cout << "# Sorting started #" << "\n";

    high_resolution_clock::time_point startTime = high_resolution_clock::now();
    vector<int> sortedArray = insertionsort.insertionSortInt(unsortedArray);
    high_resolution_clock::time_point endTime = high_resolution_clock::now();

    // -- CHANGE ABOVE THIS COMMENT --
    cout << "# Sorting ended #" << "\n";

    auto sortingDuration = duration_cast<milliseconds>(endTime - startTime);
    cout << sortingDuration.count() << "\n";

    return 0;
}
